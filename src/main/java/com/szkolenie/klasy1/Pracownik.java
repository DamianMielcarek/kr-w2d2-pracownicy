package com.szkolenie.klasy1;

public abstract class Pracownik {
    private String name;

    public Pracownik(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
