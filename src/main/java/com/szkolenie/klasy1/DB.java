package com.szkolenie.klasy1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DB {
    public static List<Pracownik> getPracownicy() {
        List<Pracownik> lista = new LinkedList<>();

        // TODO: Utworzenie obiektow Menedzer i pracownik liniowy
        // TODO: Dodanie ich do listy
        Menedzer m1 = new Menedzer("Jan Kowalski");
        lista.add(m1);
        Menedzer m2 = new Menedzer("Janina Nowak");
        lista.add(m2);

        PracownikLiniowy p1 = new PracownikLiniowy("Zenon Kapusta");
        lista.add(p1);
        PracownikLiniowy p2 = new PracownikLiniowy("Henryka Gwóźdź");
        lista.add(p2);
        PracownikLiniowy p3 = new PracownikLiniowy("Ignacy Kowalski");
        lista.add(p3);
        PracownikLiniowy p4 = new PracownikLiniowy("Agnieszka Bąk");
        lista.add(p4);
        PracownikLiniowy p5 = new PracownikLiniowy("Iwona Wozniak");
        lista.add(p5);
        PracownikLiniowy p6 = new PracownikLiniowy("Tomasz Grabarczyk");
        lista.add(p6);

        // Połączenie pracowników z ich menedżerami
        m1.setPracownicy(Arrays.asList(p1, p2));
        m2.setPracownicy(Arrays.asList(p3, p4, p5, p6));

        // Ustawienie rejestrów pracowników
        p1.setGodziny(8);
        p1.setRejestr(Arrays.asList("Z1", "Z2", "Sprzątanie"));

        p2.setGodziny(168);
        p2.setRejestr(Arrays.asList("Malowanie", "Odkurzanie"));

        p3.setGodziny(16);
        p3.setRejestr(Arrays.asList("Gotowanie"));

        p4.setGodziny(2);
        p4.setRejestr(Arrays.asList("Roznoszenie ulotek", "Grabienie liści"));

        p6.setGodziny(80);
        p6.setRejestr(Arrays.asList("Programowanie"));

        return lista;
    }

    public static List<Menedzer> getMenedzerowie() {
        List<Menedzer> menedzers = new LinkedList<>();
        for (Pracownik pr : getPracownicy()) {
            if (pr instanceof Menedzer) {
                menedzers.add((Menedzer) pr);
            }
        }
        return menedzers;
    }
}
