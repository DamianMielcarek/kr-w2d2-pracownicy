package com.szkolenie.klasy1;

import java.util.LinkedList;
import java.util.List;

public class PracownikLiniowy extends Pracownik {
    private int godziny;
    private List<String> rejestr = new LinkedList<>();

    public PracownikLiniowy(String name) {
        super(name);
    }

    public int getGodziny() {
        return godziny;
    }

    public void setGodziny(int godziny) {
        this.godziny = godziny;
    }

    public List<String> getRejestr() {
        return rejestr;
    }

    public void setRejestr(List<String> rejestr) {
        this.rejestr = rejestr;
    }

    @Override
    public String toString() {
        return String.format("%s - Pracownik liniowy", getName());
    }
}
